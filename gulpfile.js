"use strict";

const gulp = require('gulp'),
  path = require('path'),
  fs = require('fs'),
  _ = require('lodash'),
  webpack = require('webpack'),
  gutil = require('gulp-util'),
  runSequence = require('run-sequence'),
  mocha = require('gulp-mocha'),
  babel = require('gulp-babel'),
  jshint = require('gulp-jshint'),
  esdoc = require('gulp-esdoc'),
  clone = require('clone'),
  webpackConfig = require('./webpack.config');

let jshintConfig = {};

require.extensions['.json'](jshintConfig, './.jshintrc');

let jshintServerConfig = jshintConfig.exports;
let jshintClientConfig = clone(jshintServerConfig);

jshintClientConfig.predef = jshintClientConfig.predef.client;
jshintServerConfig.predef = jshintServerConfig.predef.server;

let packageJson = require('./package');

gulp.task('default', ['build']);

const reserved = ['test', 'start', 'default'];

const assetPath = 'assets/**/*';

const srcPath = 'src/**/*.js';

jshint.client = jshint.bind(null, jshintClientConfig);
jshint.server = jshint.bind(null, jshintServerConfig);

function buildTestTask(testFramework) {
  return `node ${path.join('node_modules', testFramework, 'bin', testFramework)}`;
}

function buildStartTask(binPath, args) {
  if (args) {
    if (!Array.isArray(args)) args = [ args ];
  } else args = [];
  return `node ${binPath} ${args.map(function (v) {
    if (!/^(["'])(?:[^\1])*\1$/.test(v) && /\s/.test(v)) return `"${v}"`;
    return v;
  }).join(' ')}`;
}

gulp.task('copy:templates', function () {
  return gulp.src('./src/**/*.html').pipe(gulp.dest('./dist'));
});

gulp.task('build:tasks', function () {
  packageJson.scripts = {};
  _.forOwn(gulp.tasks, function (value, key, obj) {
    if (~reserved.indexOf(key)) return;
    packageJson.scripts[key] = `node ${path.join('node_modules', 'gulp', 'bin', 'gulp')} ${key}`;
  });
  packageJson.scripts.test = buildTestTask('mocha');
  packageJson.scripts.start = buildStartTask(path.join('dist', 'bin', 'wonderbot'));
  packageJson.scripts['start:debug'] =  buildStartTask(path.join('dist', 'bin', 'wonderbot'), '--debug');
  fs.writeFileSync('./package.json', JSON.stringify(packageJson, null, 1));
});

gulp.task('build', function (cb) {
  runSequence(['build:private', 'jshint', 'test'], cb);
});

gulp.task('watch', ['default'], function () {
  return gulp.watch([ assetPath, srcPath ], ['default']);
});

gulp.task('build:private', ['build:server', 'build:client']);

gulp.task('test', function () {
  return gulp.src('test/test.js', { read: false })
    .pipe(mocha({ reporter: 'nyan' }));
});

gulp.task('build:server', function () {
  gulp.src('src/**/*.js')
    .pipe(babel({ presets: ['es2015'] }))
    .pipe(gulp.dest('dist'));
});

function copyIndex() {
  return gulp.src('./assets/index.html')
    .pipe(gulp.dest('./public/'));
}

gulp.task('build:client', ['webpack'], copyIndex);

gulp.task('build:client:dev', ['webpack:dev'], copyIndex);

function doWebpack(next) {
  webpack(webpackConfig, function (err, stats) {
    if (err) throw new gutil.PluginError('webpack', err);
    gutil.log('[webpack]', stats.toString({
      colors: true,
      chunkModules: false
    }));
    next();
  });
}

function doWebpackUnmin(next) {
  webpackConfig.plugins.splice(webpackConfig.plugins.findIndex((v) => {
    return v instanceof webpack.optimize.UglifyJsPlugin;
  }), 1);
  doWebpack(next);
}

gulp.task('webpack', doWebpack);

gulp.task('webpack:dev', doWebpackUnmin);

gulp.task('jshint', ['jshint:server', 'jshint:client']);

gulp.task('build:docs', function () {
  return gulp.src(['./src/**', './assets/**'])
    .pipe(esdoc({ destination: './docs' }));
});

gulp.task('jshint:server', function () {
  return gulp.src('./src/**/*.js')
    .pipe(jshint.server())
    .pipe(jshint.reporter('jshint-stylish'));
});
gulp.task('jshint:client', function () {
  return gulp.src('./assets/**/*.js')
    .pipe(jshint.client())
    .pipe(jshint.reporter('jshint-stylish'));
});
