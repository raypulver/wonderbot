"use strict";

var _util = require('../lib/util');

var _path = require('path');

var _stacktraceParser = require('stacktrace-parser');

var _stacktraceParser2 = _interopRequireDefault(_stacktraceParser);

var _log = require('../lib/log');

var _log2 = _interopRequireDefault(_log);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (server) {
  var oldLog = server.log;
  function pluginLog(data) {
    server.log('[plugin:core] ' + data);
  }
  function error(msg) {
    pluginLog('error: ' + msg);
  }
  function logPluginError(plugin, msg) {
    pluginLog('error in \'' + plugin + '\': ' + msg);
    pluginLog('shutting down ' + plugin + ' gracefully');
  }
  function forEach(arr, fn, thisArg) {
    try {
      arr.forEach(function (v, i, arr) {
        if (fn.call(thisArg, v, i, arr) === false) throw Error('@EarlyExit');
      });
      return true;
    } catch (e) {
      if (e.message === '@EarlyExit') return false;
      throw e;
    }
  }
  function find(arr, fn, thisArg) {
    var retval = null;
    forEach(arr, function (v, i, arr) {
      if (fn.call(thisArg, v, i, arr)) {
        retval = v;
        return false;
      }
    });
    return retval;
  }
  function handleException(err) {
    var parsed = _stacktraceParser2.default.parse(err.stack),
        name = void 0;
    console.log(require('util').inspect(parsed));
    var record = find(parsed, function (v) {
      var parsed = (0, _path.parse)(v.file);
      if (server.plugins[name = parsed.name] && (0, _path.parse)(parsed.dir).base === 'plugins') {
        return true;
      }
    });
    if (record) {
      _log2.default.processError(err);
      logPluginError(name, err.message);
      server.plugins[name].unmount();
      delete require.cache[require.resolve((0, _path.join)(__dirname, name))];
      delete server.plugins[name];
    } else throw err;
  }
  var stored;
  function parseLine(from, msg) {
    if (msg === '#more' && typeof stored === 'string') {
      server.log(stored);
    }
  }
  return {
    mount: function mount() {
      server.log = function (msg) {
        if (!msg) return;
        var split = void 0;
        if ((split = msg.split('\n')).length < 5) {
          oldLog.call(server, msg);
          stored = void 0;
        } else {
          var _split = msg.split('\n');
          _split[4] += '... (#more)';
          oldLog.call(server, _split.splice(0, 5).join('\n'));
          stored = _split.join('\n');
        }
      };
      server.addChatListener(parseLine);
      process.on('uncaughtException', handleException);
    },
    unmount: function unmount() {
      server.log = oldLog;
      server.removeChatListener(parseLine);
      _util.spliceHandler.call(process, 'uncaughtException', handleException);
    }
  };
};