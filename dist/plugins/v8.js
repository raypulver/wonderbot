"use strict";

var _dir = require('../lib/dir');

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _path = require('path');

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _fs = require('fs');

var _pathExists = require('path-exists');

var _pathExists2 = _interopRequireDefault(_pathExists);

var _vm = require('vm');

var _util = require('util');

var _babelCore = require('babel-core');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (server) {
  var ctx = void 0;
  if (_pathExists2.default.sync((0, _path.join)((0, _dir.getHomeDir)(), 'v8-state.json'))) {
    try {
      ctx = require((0, _path.join)((0, _dir.getHomeDir)(), 'v8-state'));
    } catch (e) {
      ctx = {};
    }
  } else ctx = {};
  ctx.server = server;
  ctx = (0, _vm.createContext)(ctx);
  Object.assign(ctx, {
    Object: Object,
    JSON: JSON,
    util: require('util')
  });
  function exec(script) {
    var retval = (0, _vm.runInContext)(script, ctx);
    //writeFile(join(getHomeDir(), 'v8-state.json'), JSON.stringify(ctx, function (key, value) { if (key === 'server') return undefined; return value; }, 1));
    return retval;
  }
  function log(data) {
    server.log('[plugin:question] ' + data);
  }
  function error(msg) {
    log('error: ' + msg);
  }
  function parseLine(from, msg) {
    msg = msg.split(' ');
    if (msg[0] === 'v8>') {
      try {
        var transformed = (0, _babelCore.transform)('"use strict"; void 0; ' + msg.slice(1).join(' '), { presets: ['es2015'] });
        execLog(exec(transformed.code));
      } catch (e) {
        execLog(e.message);
      }
    }
  }
  function execLog(data) {
    server.log('>> ' + (0, _util.inspect)(data, { depth: 2 }));
    return data;
  }
  return {
    mount: function mount() {
      server.addChatListener(parseLine);
    },
    unmount: function unmount() {
      server.removeChatListener(parseLine);
    }
  };
};