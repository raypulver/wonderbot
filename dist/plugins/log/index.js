'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _dir = require('../../lib/dir');

var _path = require('path');

var _http = require('http');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PORT = 8084;
var HOST = "0.0.0.0";

module.exports = function (server) {
  var app = (0, _express2.default)();
  var httpServer = (0, _http.createServer)(app);
  var sequelize = new _sequelize2.default({
    dialect: 'sqlite',
    storage: (0, _path.join)((0, _dir.getHomeDir)(), 'log.db'),
    omitNull: true,
    sync: {
      force: false
    }
  });
  var Message = sequelize.define('message', {
    id: {
      type: _sequelize2.default.INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    from: _sequelize2.default.STRING,
    msg: _sequelize2.default.TEXT
  });
  function parseLine(from, msg) {
    Message.create({
      from: from,
      msg: msg
    });
  }
  var router = _express2.default.Router();
  router.use('/', _express2.default.static((0, _path.join)(__dirname, '/')));
  app.use('/', router);
  app.get('/log', function (req, res) {
    Message.findAll({
      order: [['createdAt', 'DESC']]
    }).then(function (messages) {
      res.json(messages.map(function (v) {
        return v.get({ plain: true });
      }));
    });
  });
  return {
    mount: function mount(done) {
      httpServer.listen(PORT, HOST, function () {
        Message.sync().then(done);
      });
      server.addChatListener(parseLine);
    },
    unmount: function unmount() {
      httpServer.close();
      server.removeChatListener(parseLine);
    }
  };
};