"use strict";

var _lodash = require('lodash');

var _dir = require('../lib/dir');

var _path = require('path');

var _pathExists = require('path-exists');

var _pathExists2 = _interopRequireDefault(_pathExists);

var _reduceOwn = require('../lib/util/reduce-own');

var _reduceOwn2 = _interopRequireDefault(_reduceOwn);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var keys = Object.keys;


module.exports = function (server) {
  function error(data) {
    server.log('[plugin:help] error ' + data);
  }
  var log = server.log.bind(server);
  function parseLine(from, data) {
    data = data.split(/\s+/);
    if (data[0] === 'help') {
      if (data.length === 1) {
        if (!server.plugins.length) return error('no plugins loaded');
        log('available help topics: ' + (0, _reduceOwn2.default)(server.plugins, function (r, v, k, i, obj) {
          r += k;
          if (i !== keys(obj).length - 1) r += ', ';
          return r;
        }, ''));
      } else {
        if (!server.plugins[data[1]]) return error('no plugin \'' + data[1] + '\' loaded');
        if (typeof server.plugins[data[1]].help !== 'function') return error('plugin \'' + data[1] + '\' is loaded but has no help file');
        return server.plugins[data[1]].help(data.slice(2).join(' '));
      }
    }
  }
  return {
    mount: function mount() {
      server.addChatListener(parseLine);
    },
    unmount: function unmount() {
      server.removeChatListener(parseLine);
    }
  };
};