"use strict";

var _dir = require('../lib/dir');

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _path = require('path');

var _util = require('util');

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _log = require('../lib/log');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (server) {
  var db = void 0,
      Fact = void 0;
  function log(data) {
    server.log('[plugin:factoid] ' + data);
  }
  function error(msg) {
    log('error: ' + msg);
  }
  var factoidRe = /^!(\S+)\s*(.*$)/;
  function parseLine(from, msg) {
    var parts = void 0;
    var preSplit = msg;
    msg = msg.split(/\s+/);
    if (parts = factoidRe.exec(preSplit)) {
      console.log(parts[1]);
      if (parts[1] === 'new') {
        if (msg.length < 3) {
          error('usage: !new [name] [fact]');
        } else Fact.create({
          keyword: msg[1],
          fact: msg.slice(2).join(' ')
        }).then(function (err) {
          log(msg[2] + ' saved');
        });
      } else if (parts[1] === 'delete') {
        if (msg.length < 2) {
          error('usage: !delete [name]');
        } else Fact.findOne({
          where: {
            keyword: msg[1]
          }
        }).then(function (model) {
          if (!model) {
            error(msg[1] + ' not found');
            return Promise.reject(false);
          } else return model.destroy();
        }).then(function (model) {
          log(msg[1] + ' deleted');
        });
      } else if (parts[1] === 'list') {
        Fact.findAll().then(function (facts) {
          server.log(facts.map(function (v) {
            return '!' + v.get('keyword') + ' ' + v.get('fact');
          }).join('\n'));
        });
      } else {
        Fact.findOne({
          where: {
            keyword: parts[1]
          }
        }).then(function (result) {
          if (!result) {
            error(parts[1] + ' not found');
          } else {
            log(result.get('keyword') + ': ' + result.get('fact'));
          }
        });
      }
    }
  }
  return {
    help: function help(msg) {
      if (!msg) {
        (0, _log.logNoDecor)('\'factoid\' ~~ Save facts for later viewage');
        (0, _log.logNoDecor)('~~ usage: !KEYWORD');
        return (0, _log.logNoDecor)('available help topics: delete, new');
      }
      msg = msg.split(' ');
      switch (msg[0]) {
        case 'delete':
          (0, _log.logNoDecor)('~~ usage: !delete KEYWORD');
          break;
        case 'new':
          (0, _log.logNoDecor)('~~ usage: !new KEYWORD FACT');
          break;
      }
    },
    mount: function mount(done) {
      db = new _sequelize2.default({
        dialect: 'sqlite',
        storage: (0, _path.join)((0, _dir.getHomeDir)(), 'factoids.db'),
        omitNull: true,
        sync: {
          force: false
        }
      });
      Fact = db.define('fact', {
        fact_id: {
          type: _sequelize2.default.INTEGER,
          primaryKey: true,
          allowNull: false,
          autoIncrement: true
        },
        keyword: _sequelize2.default.STRING,
        fact: _sequelize2.default.TEXT
      });
      db.sync().then(done.bind(null, null)).catch(done);
      server.addChatListener(parseLine);
    },
    unmount: function unmount() {
      if (db) db.close();
      server.removeChatListener(parseLine);
    }
  };
};