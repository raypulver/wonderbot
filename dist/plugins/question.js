"use strict";

var _dir = require('../lib/dir');

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _path = require('path');

var _util = require('util');

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (server) {
  var db = void 0,
      Question = void 0;
  var order = [['createdAt', 'ASC']];
  function log(data) {
    server.log('[plugin:question] ' + data);
  }
  function error(msg) {
    log('error: ' + msg);
  }
  function parseLine(from, msg) {
    var parts = void 0,
        subparts = void 0,
        split = msg.split(/\s+/);
    if (parts = /^\?(\S+)(?:\s+(.*$))?/.exec(msg)) {
      if (parts[1] === 'pending') {
        Question.findAll({
          where: {
            to: from,
            pending: true
          },
          order: order
        }).then(function (qs) {
          if (!qs.length) log('no pending questions');
          qs.forEach(function (v, i) {
            log(i + 1 + ') ' + v.get('from') + ' (' + (0, _moment2.default)(v.get('createdAt')).format('M-DD h:mm') + '): ' + v.get('question'));
          });
        });
      } else if (parts[1] === 'outstanding') {
        Question.findAll({
          where: {
            from: from,
            pending: true
          },
          order: order
        }).then(function (qs) {
          if (!qs.length) log('no outstanding questions');
          qs.forEach(function (v, i) {
            log(i + ') ' + v.get('from') + ' (' + (0, _moment2.default)(v.get('createdAt')).format('M-DD h:mm') + '): ' + v.get('question'));
          });
        });
      } else if (parts[1] === 'answers') {
        Question.findAll({
          where: {
            from: from,
            pending: false,
            cleared: false
          },
          order: order
        }).then(function (qs) {
          if (!qs.length) return log('no new answers');
          qs.forEach(function (v, i) {
            server.log(i + 1 + ') (to ' + v.get('to') + ')');
            server.log(' -- Q (asked on ' + (0, _moment2.default)(v.get('createdAt')).format('M-DD') + '):  "' + v.get('question') + '"');
            server.log(' -- A (answered on ' + (0, _moment2.default)(v.get('updatedAt')).format('M-DD') + '):  "' + v.get('answer') + '"');
          });
        });
      } else if (parts[1] === 'retract') {
        if (!split[1]) return error('usage: ?retract [number]');
        Question.findAll({
          where: {
            from: from,
            pending: true
          },
          order: order,
          limit: +split[1]
        }).then(function (qs) {
          if (!qs[qs.length - 1]) return error('question ' + +split[1] + ' nonexistent');
          return qs[qs.length - 1].destroy();
        }).then(function () {
          log('question ' + +split[1] + ' deleted');
        });
      } else if (subparts = /^!(\d+)$/.exec(parts[1])) {
        if (split.length < 2) return error('must supply answer');
        Question.findAll({
          where: {
            to: from,
            pending: true
          },
          order: order,
          limit: +subparts[1]
        }).then(function (qs) {
          if (qs.length < +subparts[1]) return error('question ' + (+subparts[1] + 1) + ' nonexistent');
          var model = qs[qs.length - 1];
          return model.update({
            answer: split.slice(1).join(' '),
            pending: false
          });
        }).then(function (model) {
          return log('question from ' + model.get('from') + ' answered');
        });
      } else if (parts[1] === 'clear') {
        if (split.length < 2) return error('usage: ?clear [number]');
        Question.findAll({
          where: {
            from: from,
            pending: false,
            cleared: false
          },
          order: order,
          limit: +split[1]
        }).then(function (qs) {
          if (qs.length < +split[1]) return error('question ' + +split[1] + ' nonexistent');
          var model = qs[qs.length - 1];
          return model.update({
            cleared: true
          });
        }).then(function () {
          log('question ' + +split[1] + ' cleared');
        });
      } else {
        if (!parts[2]) {
          return error('must supply question');
        }
        Question.create({
          from: from,
          to: parts[1],
          pending: true,
          question: split.slice(1).join(' ')
        }).then(function (qs) {
          log('question posed to ' + parts[1]);
        });
      }
    }
  }
  return {
    mount: function mount(done) {
      db = new _sequelize2.default({
        dialect: 'sqlite',
        storage: (0, _path.join)((0, _dir.getHomeDir)(), 'question.db'),
        omitNull: true,
        sync: {
          force: false
        }
      });
      Question = db.define('question', {
        id: {
          type: _sequelize2.default.INTEGER,
          primaryKey: true,
          allowNull: false,
          autoIncrement: true
        },
        question: _sequelize2.default.TEXT,
        cleared: {
          type: _sequelize2.default.BOOLEAN,
          defaultValue: false
        },
        pending: _sequelize2.default.BOOLEAN,
        answer: _sequelize2.default.TEXT,
        from: _sequelize2.default.STRING,
        to: _sequelize2.default.STRING
      });
      db.sync().then(done.bind(null, null)).catch(done);
      server.addChatListener(parseLine);
    },
    unmount: function unmount() {
      if (db) db.close();
      server.removeChatListener(parseLine);
    }
  };
};