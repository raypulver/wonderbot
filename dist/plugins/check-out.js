"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _dir = require('../lib/dir');

var _path = require('path');

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _http = require('http');

var _async = require('async');

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var server = void 0;
var httpServer = void 0;
var app = void 0;

var PORT = 50001;
var HOST = "0.0.0.0";

module.exports = function (s) {
  server = s;
  function log(data) {
    server.log(formatName() + ' ' + data);
  }
  var logNoDecor = server.log.bind(server);
  return {
    help: function help(msg) {
      if (!msg) {
        logNoDecor('\'check-out\' ~~ Save links for later viewage');
        logNoDecor('~~ usage: check-out LINK [@ALIAS]');
        return logNoDecor('available help topics: list, delete');
      }
      msg = msg.split(' ');
      switch (msg[0]) {
        case 'list':
          return logNoDecor('~~ usage: check-out list');
          break;
        case 'delete':
          return logNoDecor('~~ usage: check-out delete ALIAS|NUMBER');
          break;
      }
    },
    mount: function mount(next) {
      app = (0, _express2.default)();
      app.use(_bodyParser2.default.urlencoded({ extended: false }));
      httpServer = (0, _http.createServer)(app);
      app.get('/:alias', function (req, res, next) {
        if (req.params.alias) {
          var Link = server.db.Link;

          Link.findOne({ where: { alias: req.params.alias } }).then(function (result) {
            if (result) {
              var url = void 0;
              if (!/^https?:\/\//.test(result.get('href'))) {
                url = 'http://' + result.get('href');
              } else url = result.get('href');
              res.redirect(url);
            } else res.sendStatus(404);
          }).catch(function (err) {
            log(err.message);
            res.sendStatus(500);
          });
        } else res.sendStatus(404);
      });
      server.db = new _sequelize2.default({
        dialect: 'sqlite',
        storage: (0, _path.join)((0, _dir.getHomeDir)(), server.config && server.config['check-out'] && server.config['check-out'].databaseFile || 'check-out.db'),
        omitNull: true,
        sync: {
          force: false
        }
      });
      server.db.Link = server.db.define('link', {
        id: {
          type: _sequelize2.default.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          validate: {
            isNull: false
          }
        },
        alias: _sequelize2.default.STRING,
        href: _sequelize2.default.STRING,
        desc: _sequelize2.default.STRING
      });
      server.addChatListener(parseLine);
      (0, _async.parallel)([function (cb) {
        server.db.sync().then(function () {
          ;
          cb();
        }).catch(function (err) {
          cb(err);
        });
      }, function (cb) {
        httpServer.listen(server.config && server.config['check-out'] && server.config['check-out'].port || PORT, HOST, function () {
          log('http server deployed on port ' + PORT);
          cb();
        });
      }], function (err) {
        next(err);
      });
    },
    unmount: function unmount() {
      if (server.db) server.db.close();
      if (httpServer) httpServer.close();
      server.removeChatListener(parseLine);
      if (server.db) delete server.db;
    }
  };
};

function formatName() {
  return '[plugin:' + (0, _path.parse)(__filename).name + ']';
}

function error(err) {
  return formatName() + ' error: ' + err;
}

function wrapInQuotesIf(bool) {
  if (bool) {
    return function (str) {
      return '"' + str + '"';
    };
  } else return function (str) {
    return str;
  };
}

function parseLine(from, msg) {
  if (server.db) {
    var Link = server.db.Link;

    var parts = void 0;
    if (parts = /^check-out(.*)$/.exec(msg)) {
      var _ret = function () {
        var cmd = parts[1].trim();
        cmd = cmd.split(/\s+/);
        var link = cmd[0];
        var desc = '';
        if (cmd.length > 0) {
          if (link === 'aliases') {
            Link.sync().then(function (model) {
              model.findAll({ order: [['createdAt', 'DESC']] }).then(function (links) {
                server.log(links.reduce(function (r, v, i, arr) {
                  var link = v.get({ plain: true });
                  r += '@' + link.alias;
                  if (i !== arr.length - 1) r += ', ';
                  return r;
                }, ''));
              });
            });
          } else if (link === 'list') {
            Link.sync().then(function (model) {
              model.findAll({ order: [['createdAt', 'DESC']] }).then(function (links) {
                server.log(formatName() + ' displaying ' + links.length + ' links');
                links.forEach(function (v, i) {
                  server.log(i + 1 + ' (' + (0, _moment2.default)(v.get('createdAt').getTime()).format('M-DD h:mm') + ') ' + v.get('href') + ' ' + wrapInQuotesIf(v.get('desc'))(v.get('desc')) + ' ' + (v.get('alias') ? ' @' + v.get('alias') : ''));
                });
              });
            });
          } else if (link === 'delete') {
            if (cmd.length !== 2) {
              server.log('~~ usage: check out delete <number|alias>');
            } else if (/^[a-zA-Z]+$/.test(cmd[1])) {
              Link.findOne({ where: { alias: cmd[1] } }).then(function (result) {
                if (result) {
                  result.destroy().then(function () {
                    server.log(formatName() + ' deleted ' + cmd[1]);
                  });
                } else server.log(formatName() + ' error: @' + cmd[1] + ' not found');
              }).catch(function (err) {
                server.log(formatName() + ' error: ' + err.message);
                log.processError(err);
              });
            } else {
              (function () {
                var idx = +cmd[1];
                Link.findAll({ limit: +cmd[1], order: [['createdAt', 'DESC']] }).then(function (links) {
                  var link = links[links.length - 1];
                  var url = void 0;
                  try {
                    url = link.get('href');
                    if (!url) url = 'something that was not a link';
                  } catch (e) {
                    url = String(idx);
                  }
                  return link.destroy().then(function () {
                    server.log(formatName() + ' deleted ' + url);
                  });
                }).catch(function (err) {
                  server.log(formatName() + ' error: ' + err.message);
                  log.processError(err);
                });
              })();
            }
          } else {
            var _ret3 = function () {
              if (cmd.length >= 1) desc = cmd.join(' ');
              var alias = void 0,
                  parts = void 0;
              if (desc && (parts = /^(.*)(?:^|\s)@([a-zA-Z0-9]+)\s*(.*)$/.exec(desc))) {
                alias = parts[2];
                link = (parts[1] + parts[3]).trim();
              } else {
                alias = null;
                error('must supply an alias');
                return {
                  v: {
                    v: void 0
                  }
                };
              }
              desc = '';
              Link.sync().then(function (model) {
                model.create({
                  href: link,
                  desc: desc,
                  alias: alias
                }).then(function (model) {
                  model.save().then(function () {
                    server.log(formatName() + ' link saved');
                  }).catch(function (err) {
                    if (err) server.log(formatName() + ' error: ' + err.message);
                  });
                }).catch(function (err) {
                  if (err) server.log(formatName() + ' error: ' + err.message);
                });
              }).catch(function (err) {
                if (err) server.log(formatName() + ' error: ' + err.message);
              });
            }();

            if ((typeof _ret3 === 'undefined' ? 'undefined' : _typeof(_ret3)) === "object") return _ret3.v;
          }
        }
      }();

      if ((typeof _ret === 'undefined' ? 'undefined' : _typeof(_ret)) === "object") return _ret.v;
    }
  }
}