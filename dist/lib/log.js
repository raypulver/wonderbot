"use strict";

var _chalk = require('chalk');

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _path = require('path');

var _pathExists = require('path-exists');

var _pathExists2 = _interopRequireDefault(_pathExists);

var _mkdirp = require('mkdirp');

var _mkdirp2 = _interopRequireDefault(_mkdirp);

var _os = require('os');

var _fs = require('fs');

var _async = require('async');

var _util = require('../lib/util');

var _injectPrototype = require('../lib/util/inject-prototype');

var _injectPrototype2 = _interopRequireDefault(_injectPrototype);

var _events = require('events');

var _expandTilde = require('expand-tilde');

var _expandTilde2 = _interopRequireDefault(_expandTilde);

var _logRotate = require('log-rotate');

var _logRotate2 = _interopRequireDefault(_logRotate);

var _dir = require('./dir');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _console = console;
var outLn = _console.log;
var errLn = _console.error;


var SINK_INTERVAL = 50;

var LOG_MAX_LINES = 5000;

var out = process.stdout.write.bind(process.stdout),
    err = process.stderr.write.bind(process.stderr);

(0, _injectPrototype2.default)(log, _events.EventEmitter.prototype);
_events.EventEmitter.call(log);

var sink = [];
var logPath = void 0,
    sync = void 0,
    config = void 0,
    running = void 0,
    appendLog = void 0,
    rotate = void 0;

var newLnRe = /(?:\r\n|\n|\r)/g;

function countLines(str) {
    return str.split(newLnRe).length;
}

log._countLines = countLines;

function addToSink(data) {
    var transformed = sinkTransform.call(sink, data);
    log.emit('add', transformed);
    if (sync) {
        try {
            appendLog(transformed);
            log._lineCount += countLines(transformed);
            if (log._lineCount > LOG_MAX_LINES) {
                rotate(function (err) {
                    if (err) log.processError(err);
                    log._lineCount = 0;
                });
            }
        } catch (e) {
            log.processError(e);
        }
    } else sink.push(transformed);
    log.isFlushing = true;
}

log.processError = function (err) {
    log.errorLn(err.message);
    log.debugLnDecor(err.stack);
};

function scheduleFlush() {
    var i = 0;
    (0, _async.mapSeries)(sink, function (v, next) {
        appendLog(v, function (err) {
            if (!err) {
                log._lineCount += countLines(v);
                if (log._lineCount > LOG_MAX_LINES) {
                    rotate(function (err) {
                        next(err);
                    });
                } else next();
            } else next(err);
        });
    }, function (err, result) {
        if (err) log.processError(err);
        log.isFlushing = false;
        sink.length = 0;
        log.emit('flush');
        if (running) setTimeout(function () {
            scheduleFlush();
        }, SINK_INTERVAL);
    });
}

log.interruptSink = function () {
    running = false;
    sync = true;
};

log.enableSink = function () {
    if (running) return;
    running = true;
    sync = false;
    scheduleFlush();
};

var decorRe = /^\[([^\s\]]+)\]/;

function sinkTransform(data) {
    data = (0, _util.stripColor)(data);
    data = data.replace(decorRe, '[$1 ' + (0, _moment2.default)().format('MM-DD hh:mm:ss') + ']');
    return data;
}

function log(data) {
    return log.writeLn(data);
}
log.writeLn = function (data) {
    outLn(data);
    addToSink(data + _os.EOL);
};

log.setDebugging = function (bool) {
    log._debugging = bool;
};

log.debug = function (data) {
    if (log._debugging) {
        log(data);
        addToSink(data);
    }
    return data;
};

log.debugNotify = function (data) {
    var message = void 0;
    if (log._debugging) {
        log(message = (0, _chalk.bold)((0, _chalk.yellow)('[debug]')) + ' ' + data);
        addToSink(message + _os.EOL);
    }
    return data;
};

log.debugLnDecor = log.debugNotify;

log.debug = function (data) {
    if (log._debugging) {
        log.write(data);
        addToSink(data);
    }
};

log.errorLnDecor = function (data) {
    var message = void 0;
    errLn(message = (0, _chalk.red)((0, _chalk.bold)('[error]')) + ' ' + data);
    addToSink(message + _os.EOL);
    return data;
};

log.errorLn = function (data) {
    errLn(data);
    addToSink(data + _os.EOL);
    return data;
};

log.fatalLnDecor = function (err) {
    var message = void 0;
    errLn(message = (0, _chalk.red)((0, _chalk.bold)('[fatal]')) + ' ' + err.message);
    addToSink(message + _os.EOL);
    if (log._debugging) {
        errLn(err.stack);
        addToSink(err.stack + _os.EOL);
    }
    return err;
};

log.fatalLn = function (err) {
    errLn(err.message);
    addToSink(err.message + _os.EOL);
    if (log._debugging) {
        errLn(err.stack);
        addToSink(err.stack + _os.EOL);
    }
    return err;
};

log.eventLnDecor = function (event, data) {
    var message = void 0;
    outLn(message = (0, _chalk.magenta)((0, _chalk.bold)('[event:' + String(event) + ']')) + ' ' + data);
    addToSink(message + _os.EOL);
    return data;
};

log.shutdown = function (code) {
    if (typeof code === 'undefined') code = 0;
    log.eventLnDecor('shutdown', (0, _moment2.default)().format('MM-DD-YYYY hh:mm:ss'));
};

log.info = function (data) {
    var message = void 0;
    out(message = '' + (0, _chalk.cyan)((0, _chalk.bold)('[info] ')) + data);
    addToSink(message);
};

log.infoLn = function (data) {
    var message = void 0;
    outLn(message = '' + (0, _chalk.cyan)((0, _chalk.bold)('[info] ')) + data);
    addToSink(message + _os.EOL);
};

log.write = function (data) {
    out(data);
    addToSink(data);
    return data;
};

(0, _dir.maybeMakeHomeDir)();
config = (0, _dir.getConfig)();

if (config.log) {
    logPath = config.log.path;
    if (logPath) logPath = (0, _expandTilde2.default)(logPath);
} else config.log = {};
if (!logPath) logPath = (0, _path.join)((0, _dir.getHomeDir)(), process.title + '.log');
if (logPath) {
    try {
        _mkdirp2.default.sync((0, _path.dirname)(logPath));
        log.isLogging = true;
        if ((0, _fs.existsSync)(logPath)) log._lineCount = countLines((0, _fs.readFileSync)(logPath, 'utf8'));else log._lineCount = 0;
    } catch (e) {
        log.isLogging = false;
        log.processError(e);
        log.infoLn('creating log at ' + (0, _chalk.green)(config.logPath) + ' failed' + _os.EOL + (0, _chalk.bold)(e.stack));
        log.infoLn('logging disabled');
    }
} else {
    log.isLogging = false;
}

running = log.isLogging;
appendLog = _fs.appendFile.bind(null, logPath);
rotate = _logRotate2.default.bind(null, logPath, { compress: config.log.gzip, count: config.log.rotate || 5 });

if (running) scheduleFlush();

module.exports = log;