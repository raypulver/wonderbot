/** @module dist/lib/util
 *  @description Utility functions for application
 */

"use strict";

var _clone = require('clone');

var _clone2 = _interopRequireDefault(_clone);

var _chalk = require('chalk');

var _chalk2 = _interopRequireDefault(_chalk);

var _lodash = require('lodash');

var _express = require('express');

var _fs = require('fs');

var _path = require('path');

var _injectPrototype = require('./util/inject-prototype');

var _injectPrototype2 = _interopRequireDefault(_injectPrototype);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var RouterProto = {
  getApp: function getApp() {
    return this._application;
  },
  getAppConfig: function getAppConfig() {
    return this.getApp().config;
  }
};

/** @description Create a router that keeps a reference to the main server and loads its routes and child routers from the directory structure
 * @param {module:dist/lib/server~Server} server The server object
 * @param {string} dir The directory containing the routes (usually __dirname)
 * @returns {external:Router} The router object
 * @see {module:dist/lib/util~loadRoutesSync}
 * @see {module:dist/lib/util~loadChildrenSync}
 */

function makeRouter(server, file) {
  var retval = (0, _express.Router)();
  retval._application = server;
  (0, _injectPrototype2.default)(retval, RouterProto);
  loadRoutesSync(retval, file);
  loadChildrenSync(server, retval, file);
  return retval;
}

/** @description Load route files from directory and attach them to router, routes are passed the instance of the router as an argument
 * @param {external:Router} router The router instance
 * @param {string} file The filepath of the router, passed in from __filename
 * @returns {undefined}
 */

function loadRoutesSync(router, file) {
  var parts = (0, _path.parse)(file);
  var _parts = parts;
  var base = _parts.base;
  var dir = _parts.dir;

  readdirSyncExclude(dir, base).forEach(function (v) {
    var path = (0, _path.join)(dir, v);
    parts = (0, _path.parse)(v);
    if (!(0, _fs.lstatSync)(path).isDirectory() && parts.ext === '.js') require(path)(router);
  });
}

/** @description Load child routers from subdirectories and attach them to router, routers are passed the instance of the server as an argument
 * @param {external:Router} router The router instance
 * @param {string} dir The directory to load from
 * @returns {undefined}
 */

function loadChildrenSync(server, router, file) {
  var parts = (0, _path.parse)(file);
  var base = parts.base;
  var dir = parts.dir;

  readdirSyncExclude(dir, base).filter(function (v) {
    return (0, _fs.lstatSync)((0, _path.join)(dir, v)).isDirectory();
  }).forEach(function (v) {
    router.use((0, _path.join)('/', v), require((0, _path.join)(dir, v))(server));
  });
}

/** @description Perform fs.readdirSync but exclude a file or list of files from results
 * @param {string} path The filepath to stat
 * @param {string|Array.<string>} exclude The file to exclude, or array of files
 * @returns {Array.<string>} The list of files in the directory
 */

function readdirSyncExclude(path, exclude) {
  if (typeof exclude === 'string') exclude = [exclude];
  return (0, _lodash.difference)((0, _fs.readdirSync)(path), exclude);
}

var nullifier = {
  open: '',
  close: ''
};

/**
 * @description
 * Explicitly disables the action of chalk, preventing color from being added
 * @returns {boolean} Returns false if chalk was already disabled
 */

function neutralizeColor() {
  if (!neutralizeColor.cache) {
    neutralizeColor.cache = (0, _clone2.default)(_chalk2.default.styles);
    Object.keys(_chalk2.default.styles).forEach(function (v) {
      Object.assign(_chalk2.default.styles[v], nullifier);
    });
    return true;
  }
  return false;
}

/**
 * @description
 * Re-enable chalk if it was previously disabled
 * @returns {boolean} Returns false if chalk was already enabled
 */

function reenableColor() {
  if (!neutralizeColor.cache) return false;
  _chalk2.default.styles = neutralizeColor.cache;
  delete neutralizeColor.cache;
  return true;
}

var colorRe = /\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]/g;

/**
 * @description Removes color codes from string using regular expression
 * @param {string} str The string to remove color codes from
 * @returns {string} The string with color codes removed
 */

function stripColor(str) {
  return str.replace(colorRe, '');
}

/**
 * @description
 * Splices out an event handler from an EventEmitter instance that does not implement EventEmitter#off
 * @returns {void}
 */

function spliceHandler(evt, fn) {
  if (Array.isArray(this._events[evt])) {
    var idx = this._events[evt].indexOf(fn);
    if (!~idx) return;
    this._events[evt].splice(idx, 1);
    return;
  }
  if (typeof this._events[evt] === 'function') delete this._events[evt];
}

/** @description Extracts the name from the filepath, without extension
 * @param {string} path
 * @returns {string} The name
 * @example
 * filepathName('/etc/ssl/openssl.cnf')
 * // 'openssl'
 */

function filepathName(path) {
  return (0, _path.parse)(path).name;
}

/** @description Extracts the base from the filepath, without directory
 * @param {string} path
 * @returns {string} The base
 * @example
 * filepathBase('/etc/ssl/openssl.cnf');
 * // 'openssl.cnf'
 */

function filepathBase(path) {
  return (0, _path.parse)(path).base;
}

function endpoint(filename) {
  return (0, _path.join)('/', filepathName(filename));
}

module.exports = {
  endpoint: endpoint,
  filepathBase: filepathBase,
  filepathName: filepathName,
  makeRouter: makeRouter,
  loadRoutesSync: loadRoutesSync,
  loadChildrenSync: loadChildrenSync,
  readdirSyncExclude: readdirSyncExclude,
  neutralizeColor: neutralizeColor,
  reenableColor: reenableColor,
  stripColor: stripColor,
  spliceHandler: spliceHandler
};