"use strict";

var _path = require('path');

var _os = require('os');

var _fs = require('fs');

var _pathExists = require('path-exists');

var _pathExists2 = _interopRequireDefault(_pathExists);

var _mkdirp = require('mkdirp');

var _mkdirp2 = _interopRequireDefault(_mkdirp);

var _log = require('./log');

var _log2 = _interopRequireDefault(_log);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getHomeDir() {
  return (0, _path.join)((0, _os.homedir)(), '.' + process.title);
}

function getConfigPath() {
  return (0, _path.join)(getHomeDir(), 'config.json');
}

function getConfig() {
  return require(getConfigPath());
}

function maybeMakeHomeDir() {
  var home = void 0,
      cfg = void 0;
  try {
    if (!_pathExists2.default.sync(home = getHomeDir())) {
      _mkdirp2.default.sync(home);
    }
    if (!(0, _fs.existsSync)(cfg = getConfigPath())) {
      (0, _fs.writeFileSync)(cfg, (0, _fs.readFileSync)((0, _path.join)(__dirname, '..', '..', 'presets', 'config.json')));
    }
  } catch (e) {
    console.error(e.stack);
    process.exit(1);
  }
}

module.exports = {
  getConfig: getConfig,
  maybeMakeHomeDir: maybeMakeHomeDir,
  getHomeDir: getHomeDir
};