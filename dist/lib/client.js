"use strict";

var _irc = require('irc');

var _injectPrototype = require('../lib/util/inject-prototype');

var _injectPrototype2 = _interopRequireDefault(_injectPrototype);

var _util = require('../lib/util');

var _events = require('events');

var _chalk = require('chalk');

var _util2 = require('util');

var _path = require('path');

var _fs = require('fs');

var _async = require('async');

var _lodash = require('lodash');

var _log2 = require('../lib/log');

var _log3 = _interopRequireDefault(_log2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isArray = Array.isArray;
var create = Object.create;
var keys = Object.keys;
var _EventEmitter$prototy = _events.EventEmitter.prototype;
var eveOn = _EventEmitter$prototy.on;
var eveEmit = _EventEmitter$prototy.emit;


function IRCClient() {
  var config = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

  if (!(this instanceof IRCClient)) return new IRCClient(config);
  _events.EventEmitter.call(this);
  this.config = config;
  if (!config.server) config.server = 'irc.freenode.net';
  if (!config.nick) config.nick = process.title;
  if (config.channels) {
    if (!isArray(config.channels)) config.channels = [config.channels];
  } else config.channels = ['#wonderbreadenterprises'];
  _log3.default.eventLnDecor('init', (0, _chalk.bold)((0, _chalk.yellow)('{' + process.title + '}')) + ' ' + (0, _chalk.cyan)('deploying...'));
  if (config.debug) {
    (function () {
      var emit = _irc.Client.prototype.emit;
      _irc.Client.prototype.emit = function () {
        _log3.default.debugLnDecor((0, _util2.inspect)(Array.from(arguments), { depth: null, colors: true }));
        emit.apply(this, arguments);
      };
    })();
  }
  this._client = new _irc.Client(config.server || 'irc.freenode.net', config.nick || process.title, {
    channels: config.channels || ['#wonderbreadenterprises']
  });
  var self = this;
  var connected = void 0;
  this.on('connect', onConnect);
  this.bindToLog();
  this.plugins = create(null);
  this.on('join', onJoin);
  function onConnect() {
    self.off('connect', onConnect);
    connected = true;
    _log3.default.eventLnDecor('connect', 'connected to server');
  }
  function onJoin() {
    self.off('join', onJoin);
    _log3.default.eventLnDecor('join', 'joined channel');
    self.send('[[ wonderbot initializing ]]');
    self.loadPlugins();
    self.loadCommands();
  }
}

(0, _injectPrototype2.default)(IRCClient.prototype, _events.EventEmitter.prototype);

var pluginHelp = function pluginHelp() {
  return undefined.error('usage: plugin [load|unload|list]');
};

var IRCClientFunctions = {
  log: function log(msg) {
    this.send(msg);
    _log3.default.eventLnDecor('chat', msg);
  },
  error: function error(msg) {
    this.send('error: ');
    _log3.default.errorLn(msg);
  },
  on: function on() {
    eveOn.apply(this._client, arguments);
  },
  off: function off() {
    _util.spliceHandler.apply(this._client, arguments);
  },
  serverOn: function serverOn() {
    eveOn.apply(this, arguments);
  },
  loadPlugins: function loadPlugins() {
    var _this = this;

    var pluginNames = void 0;
    var thisServer = this,
        self = this;
    if ((pluginNames = keys(this.plugins)).length) {
      this.log('~~ reloading plugins');
      (0, _async.mapSeries)(pluginNames, function (v, next) {
        try {
          _this.plugins[v].unmount(function (err) {
            if (!err) delete require.cache[require.resolve((0, _path.join)(__dirname, '..', 'plugins', v))];
            return next(err);
          });
          if (!_this.plugins[v].unmount.length) next();
        } catch (e) {
          delete require.cache[require.resolve((0, _path.join)(__dirname, '..', 'plugins', v))];
          e.plugin = v;
          next(e);
        }
      }, function (err) {
        if (err) {
          handleUnloadError.call(_this, err, err.plugin);
        } else requireAndMountAll(self, handleMount);
      });
    } else requireAndMountAll(self, handleMount);
    function handleMount(err) {
      if (err) {
        thisServer.log('~~! plugin mount error in \'' + (err.plugin || 'unknown') + '\'');
        thisServer.log(err.message);
        _log3.default.errorLn(err.stack);
      } else thisServer.log('~~ plugins loaded');
    }
  },
  loadCommands: function loadCommands() {
    var _this2 = this;

    this.addChatListener(function (from, message) {
      var split = message.split(/\s+/);
      if (split[0] === 'plugin') {
        if (split.length === 1) pluginHelp.call(_this2);else if (split[1] === 'list') _this2.logPluginList();else if (split[1] === 'load') {
          if (split.length === 2) _this2.loadPlugins();else split.slice(2).forEach(function (v) {
            _this2.loadPlugin(v);
          });
        } else if (split[1] === 'unload') {
          if (split.length === 2) (0, _lodash.forOwn)(_this2.plugins, function (value, key) {
            _this2.unloadPlugin(key);
          });else split.slice(2).forEach(function (v) {
            _this2.unloadPlugin(v);
          });
        } else pluginHelp.call(_this2);
      } else if (split[0] === 'unload') {
        if (split.length > 1) {
          split.slice(1).forEach(function (v) {
            _this2.unloadPlugin(v);
          });
        }
      }
    });
  },
  unloadPlugin: function unloadPlugin(plugin) {
    var _this3 = this;

    if (this.plugins[plugin]) {
      this.log('~~ unmounting \'' + plugin + '\'');
      try {
        this.plugins[plugin].unmount(function (err) {
          handleUnloadError.call(_this3, err, plugin);
        });
        if (!this.plugins[plugin].unmount.length) {
          handleUnloadError.call(this, null, plugin);
        }
      } catch (e) {
        handleUnloadError.call(this, e, plugin);
      }
    } else {
      this.log('~~! no plugin \'' + plugin + '\' active');
    }
  },
  loadPlugin: function loadPlugin(plugin) {
    var _this4 = this;

    var self = this;
    if (this.plugins[plugin]) {
      this.log('~~ \'' + plugin + '\' already mounted');
      try {
        this.plugins[plugin].unmount(function (err) {
          handleUnloadError.call(_this4, err, plugin);
          if (!err) requireAndMount(_this4, plugin, handleMount);
        });
        if (!this.plugins[plugin].unmount.length) {
          handleUnloadError.call(this, null, plugin);
          requireAndMount(this, plugin, handleMount);
        }
      } catch (e) {
        handleUnloadError.call(this, e, plugin);
      }
    } else requireAndMount(this, plugin, handleMount);
    function handleMount(err) {
      if (err) {
        self.log('~~! plugin mount error in \'' + (err.plugin || 'unknown') + '\'');
        self.log(err.message);
        _log3.default.errorLn(err.stack);
      } else self.log('~~ plugins loaded');
    }
  },
  send: function send(name, msg) {
    var _this5 = this;

    if (!msg) {
      msg = name;
      this.config.channels.forEach(function (v) {
        _this5._client.say(v, msg);
      });
    } else this._client.say(name, msg);
  },
  addChatListener: function addChatListener(fn) {
    var _this6 = this;

    this.config.channels.forEach(function (v) {
      _this6.on('message' + v, fn);
    });
  },
  removeChatListener: function removeChatListener(fn) {
    var _this7 = this;

    this.config.channels.forEach(function (v) {
      _this7.off('message' + v, fn);
    });
  },
  bindToLog: function bindToLog() {
    this.on('error', function (err) {
      _log3.default.errorLn(err.message);
      _log3.default.debugLnDecor(err.stack);
    });
  },
  logPluginList: function logPluginList() {
    var _this8 = this;

    this.log('~~ available plugins');
    (0, _fs.readdirSync)((0, _path.join)(__dirname, '..', 'plugins')).forEach(function (value) {
      var parts = (0, _path.parse)(value);
      if (parts.ext === '.js' || (0, _fs.lstatSync)((0, _path.join)(__dirname, '..', 'plugins', value)).isDirectory()) _this8.log(' * ' + parts.name + ' ' + (_this8.plugins[parts.name] && '(active)' || ''));
    });
  }
};
function handleUnloadError(err, plugin) {
  if (err) {
    this.log('~~! ' + plugin + ' unmount error: ' + err.message);
    this.log(' ~! WARNING: forcing plugin shutdown');
    _log3.default.processError(err);
  }
  delete require.cache[require.resolve((0, _path.join)(__dirname, '..', 'plugins', plugin))];
}

function handleErr(err, plugin) {
  if (err) {
    err.plugin = plugin;
    this.log('~~! error loading \'' + err.plugin + '\': ' + err.message);
    _log3.default.processError(err);
  }
}

function requireAndMountAll(server, cb) {
  var pluginDir = void 0;
  var plugins = (0, _fs.readdirSync)(pluginDir = (0, _path.join)(__dirname, '..', 'plugins')).filter(function (v) {
    return (0, _path.parse)(v).ext === '.js' || (0, _fs.lstatSync)((0, _path.join)(pluginDir, v)).isDirectory();
  }).map(function (v) {
    return (0, _path.parse)(v).name;
  });
  (0, _async.mapSeries)(plugins, function (v, next) {
    server.plugins[v] = require((0, _path.join)(__dirname, '..', 'plugins', v))(server);
    try {
      server.plugins[v].mount(function (err) {
        if (!err) server.log(' ** \'' + v + '\' loaded');else {
          handleErr.call(server, err, v);
        }
        next();
      });
      if (!server.plugins[v].mount.length) {
        server.log(' ** \'' + v + '\' loaded');
        next();
      }
    } catch (e) {
      handleErr.call(server, e, v);
      next();
    }
  }, function (err) {
    cb(err);
  });
}

function requireAndMount(server, plugin, cb) {
  if (!(0, _fs.existsSync)((0, _path.join)(__dirname, '..', 'plugins', plugin + '.js')) && !(0, _fs.existsSync)((0, _path.join)(__dirname, '..', 'plugins', plugin))) {
    server.log(' ** plugin \'' + plugin + '\' not found');
    server.logPluginList();
  } else {
    try {
      server.plugins[plugin] = require((0, _path.join)(__dirname, '..', 'plugins', plugin))(server);
      server.plugins[plugin].mount(function (err) {
        if (!err) server.log(' ** \'' + plugin + '\' loaded');
        if (err) err.plugin = plugin;
        cb(err);
      });
      if (!server.plugins[plugin].mount.length) {
        server.log(' ** \'' + plugin + '\' loaded');
        cb();
      }
    } catch (e) {
      e.plugin = plugin;
      cb(e);
    }
  }
}

Object.assign(IRCClient.prototype, IRCClientFunctions);

module.exports = IRCClient;