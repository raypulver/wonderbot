/** @module dist/lib/util/inject-prototype
 * @description Inject prototype into an object
 */

"use strict";

var _create = require("./create");

var _create2 = _interopRequireDefault(_create);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setPrototypeOf = Object.setPrototypeOf;
var getPrototypeOf = Object.getPrototypeOf;

/** @description Inject a prototype between the object and its current object, setting the current prototype to be the prototype of the injected prototype
 * @param {Object} target The object to modify
 * @param {Object} proto The prototype to inject
 * @returns {Object} returns its first argument
 */

function injectPrototype(target, proto) {
  return setPrototypeOf(target, (0, _create2.default)(getPrototypeOf(target), proto));
}

function forEachProto(obj, cb, thisArg) {
  var i = 0;
  var retval = true;
  while (obj = getPrototypeOf(obj)) {
    if (cb.call(thisArg, obj, i) === false) {
      retval = false;
      break;
    }
    ++i;
  }
  return retval;
}

module.exports = injectPrototype;