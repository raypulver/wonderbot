/** @module dist/lib/util/create
 * @description Create an object with a given prototype
 */

"use strict";

var _mapOwn = require("./map-own");

var _mapOwn2 = _interopRequireDefault(_mapOwn);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var create = Object.create;
var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;


module.exports = function (proto, obj) {
  return create(proto, (0, _mapOwn2.default)(obj, function (v, key, obj) {
    return getOwnPropertyDescriptor(obj, key);
  }));
};