/** @module dist/lib/util/reduce-own
 * @description Reduce objects own properties to new value
 */

"use strict";

var _lodash = require("lodash");

/** @description Reduce object to a new object, similar to Array#reduce
 * @param {Object} object The object to map
 * @param {Function} cb The callback function
 * @param {Any} initial The initial value
 * @returns {Any} Final value
 */

var create = Object.create;
var getPrototypeOf = Object.getPrototypeOf;


function reduceOwn(object, cb, initial) {
  var i = 0;
  (0, _lodash.forOwn)(object, function (val, key) {
    initial = cb(initial, val, key, i, object);
    i++;
  });
  return initial;
}

module.exports = reduceOwn;