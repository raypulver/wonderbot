/** @module dist/lib/util/map-own
 * @description Map objects own properties to new object
 */

"use strict";

var _lodash = require("lodash");

/** @description Map object to a new object, similar to Array#map
 * @param {Object} object The object to map
 * @param {Function} cb The callback function
 * @returns {Object} New object
 */

var create = Object.create;
var getPrototypeOf = Object.getPrototypeOf;


function mapOwn(object, cb) {
  var retval = create(getPrototypeOf(object));
  (0, _lodash.forOwn)(object, function (val, key) {
    retval[key] = cb(val, key, object);
  });
  return retval;
}

module.exports = mapOwn;