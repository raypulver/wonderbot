"use strict";

var _path = require('path');

var _client = require('../lib/client');

var _client2 = _interopRequireDefault(_client);

var _nodeGetopt = require('node-getopt');

var _nodeGetopt2 = _interopRequireDefault(_nodeGetopt);

var _log = require('../lib/log');

var _log2 = _interopRequireDefault(_log);

var _chalk = require('chalk');

var _os = require('os');

var _dir = require('../lib/dir');

var _package = require('../../package');

var _package2 = _interopRequireDefault(_package);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

process.title = (0, _path.parse)(process.argv[1]).name;

var _process = process;
var exit = _process.exit;


var getopt = new _nodeGetopt2.default([['p', 'port=PORT', 'http port'], ['d', 'debug', 'enable debugging output'], ['h', 'help', 'display this help'], ['v', 'version', 'display version number']]);

getopt.setHelp((0, _chalk.yellow)((0, _chalk.bold)('Usage:')) + ' ' + (0, _chalk.yellow)((0, _chalk.bold)(process.title)) + ' ' + (0, _chalk.yellow)((0, _chalk.bold)('[-dvh] [-p PORT]')) + _os.EOL + (0, _chalk.yellow)('Options are:') + _os.EOL + (0, _chalk.gray)((0, _chalk.bold)('[[OPTIONS]]')));

var opts = getopt.parseSystem();

if (opts.options.help) {
  getopt.showHelp();
  exit(0);
}

if (opts.options.version) {
  (0, _log2.default)(_package2.default.version);
  exit(0);
}

(0, _dir.maybeMakeHomeDir)();

var config = (0, _dir.getConfig)();

config.debug = opts.options.debug;
_log2.default.setDebugging(opts.options.debug);
if (opts.options.port) config.port = opts.options.port;

var irc = (0, _client2.default)(config);

irc.addChatListener(function (from, message) {
  _log2.default.infoLn(from + ' => ' + message);
});