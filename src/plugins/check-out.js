"use strict";

import Sequelize from 'sequelize';
import { getHomeDir } from '../lib/dir';
import { join, parse } from 'path';
import moment from 'moment';
import express from 'express';
import { createServer } from 'http';
import { parallel } from 'async';
import bodyParser from 'body-parser';

let server;
let httpServer;
let app;

const PORT = 50001;
const HOST = "0.0.0.0";


module.exports = function (s) {
  server = s;
  function log (data) {
    server.log(`${formatName()} ${data}`);
  }
  const logNoDecor = server.log.bind(server);
  return {
    help(msg) {
      if (!msg) {
        logNoDecor(`'check-out' ~~ Save links for later viewage`);
        logNoDecor('~~ usage: check-out LINK [@ALIAS]');
        return logNoDecor('available help topics: list, delete');
      }
      msg = msg.split(' ');
      switch (msg[0]) {
        case 'list':
          return logNoDecor(`~~ usage: check-out list`);
          break;
        case 'delete':
          return logNoDecor(`~~ usage: check-out delete ALIAS|NUMBER`);
          break;
      }
    },
    mount(next) {
      app = express();
      app.use(bodyParser.urlencoded({ extended: false }));
      httpServer = createServer(app);
      app.get('/:alias', function (req, res, next) {
        if (req.params.alias) {
          const { Link } = server.db;
          Link.findOne({ where: { alias: req.params.alias } }).then((result) => {
            if (result) {
              let url;
              if (!/^https?:\/\//.test(result.get('href'))) {
                url = 'http://' + result.get('href');
              } else url = result.get('href');
              res.redirect(url);
            } else res.sendStatus(404);
          }).catch((err) => {
            log(err.message);           
            res.sendStatus(500); 
          });
        } else res.sendStatus(404);
      });
      server.db = new Sequelize({
        dialect: 'sqlite',
        storage: join(getHomeDir(), (server.config && server.config['check-out'] && server.config['check-out'].databaseFile || 'check-out.db')),
        omitNull: true,
        sync: {
          force: false
        }
      });
      server.db.Link = server.db.define('link', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          validate: {
            isNull: false
          }
        },
        alias: Sequelize.STRING,
        href: Sequelize.STRING,
        desc: Sequelize.STRING
      });
      server.addChatListener(parseLine);
      parallel([function (cb) {
        server.db.sync().then(() => {;
          cb();
        }).catch((err) => {
          cb(err);
        });
      }, function (cb) {
        httpServer.listen(server.config && server.config['check-out'] && server.config['check-out'].port || PORT, HOST, function () {
          log(`http server deployed on port ${PORT}`);
          cb();
        });
      }], (err) => {
        next(err);
      });
    },
    unmount() {
      if (server.db) server.db.close();
      if (httpServer) httpServer.close();
      server.removeChatListener(parseLine);
      if (server.db) delete server.db;
    }
  };
}


function formatName() {
  return `[plugin:${parse(__filename).name}]`;
}

function error(err) {
  return `${formatName()} error: ${err}`;
}

function wrapInQuotesIf(bool) {
  if (bool) {
    return function (str) {
      return '"' + str + '"';
    }
  } else return function (str) { return str; }
}

function parseLine(from, msg) {
  if (server.db) {
  const { Link } = server.db;
  let parts;
  if ((parts = /^check-out(.*)$/.exec(msg))) {
    let cmd = parts[1].trim();
    cmd = cmd.split(/\s+/);
    let link = cmd[0];
    let desc = '';
    if (cmd.length > 0) { 
      if (link === 'aliases') {
        Link.sync().then((model) => {
          model.findAll({ order: [['createdAt', 'DESC']] }).then((links) => {
            server.log(links.reduce((r, v, i, arr) => {
              let link = v.get({ plain: true });
              r += '@' + link.alias;
              if (i !== arr.length - 1) r += ', ';
              return r;
            }, ''));
          });
        });
      } else if (link === 'list') {
        Link.sync().then((model) => {
          model.findAll({ order: [['createdAt', 'DESC']] }).then((links) => {
            server.log(`${formatName()} displaying ${links.length} links`);
            links.forEach((v, i) => {
               server.log(`${i + 1} (${moment(v.get('createdAt').getTime()).format('M-DD h:mm')}) ${v.get('href')} ${wrapInQuotesIf(v.get('desc'))(v.get('desc'))} ${v.get('alias') ? ` @${v.get('alias')}` : ''}`);
            });
          });
        });
      } else if (link === 'delete') {
       if (cmd.length !== 2) {
         server.log(`~~ usage: check out delete <number|alias>`);
       } else if (/^[a-zA-Z]+$/.test(cmd[1])) {
         Link.findOne({ where: { alias: cmd[1] } }).then((result) => {
           if (result) {
             result.destroy().then(() => {
               server.log(`${formatName()} deleted ${cmd[1]}`);
             });
           } else server.log(`${formatName()} error: @${cmd[1]} not found`);
         }).catch((err) => {
           server.log(`${formatName()} error: ${err.message}`);
           log.processError(err);
         });
       } else {
         let idx = +cmd[1];
         Link.findAll({ limit: +cmd[1], order: [['createdAt', 'DESC']] }).then((links) => {
           let link = links[links.length - 1];
           let url;
           try {
             url = link.get('href');
             if (!url) url = 'something that was not a link';
           } catch (e) {
             url = String(idx);
           }
           return link.destroy().then(() => {
             server.log(`${formatName()} deleted ${url}`);
           });
         }).catch((err) => {
           server.log(`${formatName()} error: ${err.message}`);
           log.processError(err);
         });
        } 
      } else {
        if (cmd.length >= 1) desc = cmd.join(' ');
        let alias, parts;
        if (desc && (parts = /^(.*)(?:^|\s)@([a-zA-Z0-9]+)\s*(.*)$/.exec(desc))) {
          alias = parts[2];
          link = (parts[1] + parts[3]).trim();
        } else {
          alias = null;
          error('must supply an alias');
          return;
        }
        desc = '';
        Link.sync().then((model) => {
          model.create({
            href: link,
            desc,
            alias
          }).then((model) => {
            model.save().then(() => {
              server.log(`${formatName()} link saved`);
            }).catch((err) => {
              if (err) server.log(`${formatName()} error: ${err.message}`);
            });
          }).catch((err) => {
            if (err) server.log(`${formatName()} error: ${err.message}`);
          });
        }).catch((err) => {
          if (err) server.log(`${formatName()} error: ${err.message}`);
        });
      }
    } 
  }
  }
}
