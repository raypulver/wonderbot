"use strict";

import { getHomeDir } from '../lib/dir';
import Sequelize from 'sequelize';
import { join } from 'path';
import moment from 'moment';
import { writeFile } from 'fs';
import pathExists from 'path-exists';
import { createContext, runInContext } from 'vm';
import { inspect } from 'util';
import { transform } from 'babel-core';

module.exports = function (server) {
  let ctx;
  if (pathExists.sync(join(getHomeDir(), 'v8-state.json'))) {
    try {
      ctx = require(join(getHomeDir(), 'v8-state'));
    } catch (e) {
      ctx = {};
    }
  } else ctx = {};
  ctx.server = server;
  ctx = createContext(ctx);
  function exec(script) {
    let retval = runInContext(script, ctx);
    writeFile(join(getHomeDir(), 'v8-state.json'), JSON.stringify(ctx, function (key, value) { if (key === 'server') return undefined; return value; }, 1));
    return retval;
  }
  function log(data) {
    server.log(`[plugin:question] ${data}`);
  }
  function error(msg) {
    log(`error: ${msg}`);
  }
  function parseLine(from, msg) {
    msg = msg.split(' ');
    if (msg[0] === '#crash') {
      throw Error('if everything worked wonderbot is still alive');
    }
  }
  function execLog(data) {
    server.log(`>> ${inspect(data, { depth: 2 })}`);
    return data;
  }
  return {
    mount() {
     server.addChatListener(parseLine);
    },
    unmount() {
      server.removeChatListener(parseLine);
    }
  };
};

