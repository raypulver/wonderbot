import express from 'express';
import Sequelize from 'sequelize';
import { getHomeDir } from '../../lib/dir';
import { join } from 'path';
import { createServer } from 'http';

const PORT = 8084;
const HOST = "0.0.0.0";

module.exports = function (server) {
  let app = express();
  let httpServer = createServer(app);
  let sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: join(getHomeDir(), 'log.db'),
    omitNull: true,
    sync: {
      force: false
    }
  });
  let Message = sequelize.define('message', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    from: Sequelize.STRING,
    msg: Sequelize.TEXT
  });
  function parseLine(from, msg) {
    Message.create({
      from,
      msg
    });
  }
  var router = express.Router();
  router.use('/', express.static(join(__dirname, '/')));
  app.use('/', router);
  app.get('/log', function (req, res) {
    Message.findAll({
      order: [['createdAt', 'DESC']]
    }).then((messages) => {
      res.json(messages.map(function (v) { return v.get({ plain: true }); }));
    });
  });
  return {
    mount(done) {
      httpServer.listen(PORT, HOST, function () {
        Message.sync().then(done);
      });
      server.addChatListener(parseLine);
    },
    unmount() {
      httpServer.close();
      server.removeChatListener(parseLine);
    }
  };
};
