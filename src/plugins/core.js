"use strict";

import { spliceHandler } from '../lib/util';
import { join, parse } from 'path';
import StackTraceParser from 'stacktrace-parser';
import log from '../lib/log';

module.exports = function (server) {
  var oldLog = server.log;
  function pluginLog(data) {
    server.log(`[plugin:core] ${data}`);
  }
  function error(msg) {
    pluginLog(`error: ${msg}`);
  }
  function logPluginError(plugin, msg) {
    pluginLog(`error in '${plugin}': ${msg}`);
    pluginLog(`shutting down ${plugin} gracefully`);
  }
  function forEach(arr, fn, thisArg) {
    try {
      arr.forEach(function (v, i, arr) {
        if (fn.call(thisArg, v, i, arr) === false) throw Error('@EarlyExit');
      });
      return true;
    } catch (e) {
      if (e.message === '@EarlyExit') return false;
      throw e;
    }
  }
  function find(arr, fn, thisArg) {
    let retval = null;
    forEach(arr, function (v, i, arr) {
      if (fn.call(thisArg, v, i,  arr)) {
        retval = v;
        return false;
      }
    });
    return retval;
  }
  function handleException(err) {
    let parsed = StackTraceParser.parse(err.stack), name;
    console.log(require('util').inspect(parsed));
    var record = find(parsed, function (v) {
      let parsed = parse(v.file);
      if (server.plugins[(name = parsed.name)] && parse(parsed.dir).base === 'plugins') {
        return true;
      }
    });
    if (record) {
      log.processError(err);
      logPluginError(name, err.message);
      server.plugins[name].unmount();
      delete require.cache[require.resolve(join(__dirname, name))];
      delete server.plugins[name];
    } else throw err;
  }
  var stored;
  function parseLine(from, msg) {
    if (msg === '#more' && typeof stored === 'string') {
      server.log(stored);
    }
  }
  return {
    mount() {
      server.log = function (msg) {
        if (!msg) return;
        let split;
        if ((split = msg.split('\n')).length < 5) {
          oldLog.call(server, msg);
          stored = void 0;
        } else {
          let split = msg.split('\n');
          split[4] += '... (#more)';
          oldLog.call(server, split.splice(0, 5).join('\n'));
          stored = split.join('\n');
        }  
      };
      server.addChatListener(parseLine);
      process.on('uncaughtException', handleException);
    },
    unmount() {
      server.log = oldLog;
      server.removeChatListener(parseLine);
      spliceHandler.call(process, 'uncaughtException', handleException);
    }
  };
};

