"use strict";

import { forOwn } from 'lodash';
import { getHomeDir } from '../lib/dir';
import { join } from 'path';
import pathExists from 'path-exists';
import reduceOwn from '../lib/util/reduce-own';

const { keys } = Object;

module.exports = function (server) {
  function error(data) {
    server.log(`[plugin:help] error ${data}`);
  }
  const log = server.log.bind(server);
  function parseLine(from, data) {
    data = data.split(/\s+/);
    if (data[0] === 'help') {
      if (data.length === 1) {
        if (!server.plugins.length) return error('no plugins loaded');
        log('available help topics: ' + reduceOwn(server.plugins, (r, v, k, i, obj) => {
          r += k;
          if (i !== keys(obj).length - 1) r += ', ';
          return r;
        }, ''));
      } else {
        if (!server.plugins[data[1]]) return error(`no plugin '${data[1]}' loaded`);
        if (typeof server.plugins[data[1]].help !== 'function') return error(`plugin '${data[1]}' is loaded but has no help file`);
        return server.plugins[data[1]].help(data.slice(2).join(' '));
      }
    }
  }
  return {
    mount() {
     server.addChatListener(parseLine);
    },
    unmount() {
      server.removeChatListener(parseLine);
    }
  };
};

