"use strict";

import { getHomeDir } from '../lib/dir';
import Sequelize from 'sequelize';
import { join } from 'path';
import { inspect } from 'util';
import moment from 'moment';
import { logNoDecor } from '../lib/log';

module.exports = function (server) {
  let db, Fact;
  function log(data) {
    server.log(`[plugin:factoid] ${data}`);
  }
  function error(msg) {
    log(`error: ${msg}`);
  }
  const factoidRe = /^!(\S+)\s*(.*$)/;
  function parseLine(from, msg) {
    let parts;
    let preSplit = msg;
    msg = msg.split(/\s+/);
    if ((parts = factoidRe.exec(preSplit))) {
      console.log(parts[1]);
      if (parts[1] === 'new') {
        if (msg.length < 3) {
          error('usage: !new [name] [fact]');
        }
        else Fact.create({
          keyword: msg[1],
          fact: msg.slice(2).join(' ')
        }).then((err) => {
          log(`${msg[2]} saved`);
        });
      } else if (parts[1] === 'delete') {
        if (msg.length < 2) {
          error('usage: !delete [name]');
        }
        else Fact.findOne({
          where: {
            keyword: msg[1]
          }
        }).then((model) => {
          if (!model) {
            error(`${msg[1]} not found`);
            return Promise.reject(false);
          }
          else return model.destroy();
        }).then((model) => {
          log(`${msg[1]} deleted`);
        });
      } else if (parts[1] === 'list') {
        Fact.findAll().then((facts) => {
          server.log(facts.map(function (v) {
            return `!${v.get('keyword')} ${v.get('fact')}`;
          }).join('\n'));
        });
      } else {
        Fact.findOne({
          where: {
            keyword: parts[1]
          }
        }).then((result) => {
          if (!result) {
            error(`${parts[1]} not found`);
          } else {
            log(`${result.get('keyword')}: ${result.get('fact')}`);
          }
        });
      }
    } 
  }
  return {
    help(msg) {
      if (!msg) {
        logNoDecor(`'factoid' ~~ Save facts for later viewage`);
        logNoDecor('~~ usage: !KEYWORD');
        return logNoDecor('available help topics: delete, new');
      }
      msg = msg.split(' ');
      switch (msg[0]) {
        case 'delete':
          logNoDecor('~~ usage: !delete KEYWORD');
          break;
        case 'new':
          logNoDecor('~~ usage: !new KEYWORD FACT');
          break;
      }
    },
    mount(done) {
      db = new Sequelize({
        dialect: 'sqlite',
        storage: join(getHomeDir(), 'factoids.db'),
        omitNull: true,
        sync: {
          force: false
        }
     });
     Fact = db.define('fact', {
       fact_id: {
         type: Sequelize.INTEGER,
         primaryKey: true,
         allowNull: false,
         autoIncrement: true
       },
       keyword: Sequelize.STRING,
       fact: Sequelize.TEXT
     });
     db.sync().then(done.bind(null, null)).catch(done);
     server.addChatListener(parseLine);
    },
    unmount() {
      if (db) db.close();
      server.removeChatListener(parseLine);
    }
  };
};

