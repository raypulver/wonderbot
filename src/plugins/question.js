"use strict";

import { getHomeDir } from '../lib/dir';
import Sequelize from 'sequelize';
import { join } from 'path';
import { inspect } from 'util';
import moment from 'moment';

module.exports = function (server) {
  let db, Question;
  let order = [['createdAt', 'ASC']];
  function log(data) {
    server.log(`[plugin:question] ${data}`);
  }
  function error(msg) {
    log(`error: ${msg}`);
  }
  function parseLine(from, msg) {
    let parts, subparts, split = msg.split(/\s+/);
    if ((parts = /^\?(\S+)(?:\s+(.*$))?/.exec(msg))) {
      if (parts[1] === 'pending') {
        Question.findAll({
          where: {
            to: from,
            pending: true
          },
          order
        }).then((qs) => {
          if (!qs.length) log('no pending questions');
          qs.forEach((v, i) => {
            log(`${i + 1}) ${v.get('from')} (${moment(v.get('createdAt')).format('M-DD h:mm')}): ${v.get('question')}`);
          });
        });
      } else if (parts[1] === 'outstanding') {
        Question.findAll({
          where: {
            from,
            pending: true
          },
          order
        }).then((qs) => {
          if (!qs.length) log('no outstanding questions');
          qs.forEach((v, i) => {
            log(`${i}) ${v.get('from')} (${moment(v.get('createdAt')).format('M-DD h:mm')}): ${v.get('question')}`);
          });
        });
      } else if (parts[1] === 'answers') {
        Question.findAll({
          where: {
            from,
            pending: false,
            cleared: false
          },
          order
        }).then((qs) => {
          if (!qs.length) return log('no new answers');
          qs.forEach((v, i) => {
            server.log(`${i + 1}) (to ${v.get('to')})`);
            server.log(` -- Q (asked on ${moment(v.get('createdAt')).format('M-DD')}):  "${v.get('question')}"`);
            server.log(` -- A (answered on ${moment(v.get('updatedAt')).format('M-DD')}):  "${v.get('answer')}"`);
          });
        });
      } else if (parts[1] === 'retract') {
        if (!split[1]) return error('usage: ?retract [number]');
        Question.findAll({
          where: {
            from,
            pending: true
          },
          order,
          limit: +split[1] 
        }).then((qs) => {
          if (!qs[qs.length - 1]) return error(`question ${+split[1]} nonexistent`);
          return qs[qs.length - 1].destroy();
        }).then(() => {
          log(`question ${+split[1]} deleted`);
        });
      } else if ((subparts = /^!(\d+)$/.exec(parts[1]))) {
        if (split.length < 2) return error('must supply answer');
        Question.findAll({
          where: {
            to: from,
            pending: true
          },
          order,
          limit: +subparts[1]
        }).then((qs) => {
          if (qs.length < +subparts[1]) return error(`question ${+subparts[1] + 1} nonexistent`);
          let model = qs[qs.length - 1];
          return model.update({
            answer: split.slice(1).join(' '),
            pending: false
          });
        }).then((model) => {
          return log(`question from ${model.get('from')} answered`);
        });
      } else if (parts[1] === 'clear') {
        if (split.length < 2) return error('usage: ?clear [number]');
        Question.findAll({
          where: {
            from,
            pending: false,
            cleared: false
          },
          order,
          limit: +split[1] 
        }).then((qs) => {
          if (qs.length < +split[1]) return error(`question ${+split[1]} nonexistent`);
          let model = qs[qs.length - 1];
          return model.update({
            cleared: true
          });
        }).then(() => {
          log(`question ${+split[1]} cleared`);
        });
      } else {
        if (!parts[2]) {
          return error('must supply question');
        }
        Question.create({
          from,
          to: parts[1],
          pending: true,
          question: split.slice(1).join(' ')
        }).then((qs) => {
          log(`question posed to ${parts[1]}`);
        });
      }
    }
  }
  return {
    mount(done) {
      db = new Sequelize({
        dialect: 'sqlite',
        storage: join(getHomeDir(), 'question.db'),
        omitNull: true,
        sync: {
          force: false
        }
     });
     Question = db.define('question', {
       id: {
         type: Sequelize.INTEGER,
         primaryKey: true,
         allowNull: false,
         autoIncrement: true
       },
       question: Sequelize.TEXT,
       cleared: {
         type: Sequelize.BOOLEAN,
         defaultValue: false
       },
       pending: Sequelize.BOOLEAN,
       answer: Sequelize.TEXT,
       from: Sequelize.STRING,
       to: Sequelize.STRING
     });
     db.sync().then(done.bind(null, null)).catch(done);
     server.addChatListener(parseLine);
    },
    unmount() {
      if (db) db.close();
      server.removeChatListener(parseLine);
    }
  };
};

