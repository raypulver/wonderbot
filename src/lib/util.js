/** @module dist/lib/util
 *  @description Utility functions for application
 */

"use strict";

import clone from 'clone';
import chalk from 'chalk';
import { forOwn, difference } from 'lodash'; 
import { Router } from 'express';
import { readdirSync, lstatSync } from 'fs';
import { join, parse } from 'path';
import injectPrototype from './util/inject-prototype';

const RouterProto = {
  getApp() {
    return this._application;
  },
  getAppConfig() {
    return this.getApp().config;
  }
};

/** @description Create a router that keeps a reference to the main server and loads its routes and child routers from the directory structure
 * @param {module:dist/lib/server~Server} server The server object
 * @param {string} dir The directory containing the routes (usually __dirname)
 * @returns {external:Router} The router object
 * @see {module:dist/lib/util~loadRoutesSync}
 * @see {module:dist/lib/util~loadChildrenSync}
 */

function makeRouter(server, file) {
  let retval = Router();
  retval._application = server;
  injectPrototype(retval, RouterProto);
  loadRoutesSync(retval, file);
  loadChildrenSync(server, retval, file);
  return retval;
}

/** @description Load route files from directory and attach them to router, routes are passed the instance of the router as an argument
 * @param {external:Router} router The router instance
 * @param {string} file The filepath of the router, passed in from __filename
 * @returns {undefined}
 */

function loadRoutesSync(router, file) {
  let parts = parse(file);
  const { base, dir } = parts;
  readdirSyncExclude(dir, base).forEach((v) => {
    let path = join(dir, v);
    parts = parse(v);
    if (!lstatSync(path).isDirectory() && parts.ext === '.js') require(path)(router);
  });
}

/** @description Load child routers from subdirectories and attach them to router, routers are passed the instance of the server as an argument
 * @param {external:Router} router The router instance
 * @param {string} dir The directory to load from
 * @returns {undefined}
 */

function loadChildrenSync(server, router, file) {
  let parts = parse(file);
  const { base, dir } = parts;
  readdirSyncExclude(dir, base).filter((v) => {
    return lstatSync(join(dir, v)).isDirectory();
  }).forEach((v) => {
    router.use(join('/', v), require(join(dir, v))(server));
  });
}

/** @description Perform fs.readdirSync but exclude a file or list of files from results
 * @param {string} path The filepath to stat
 * @param {string|Array.<string>} exclude The file to exclude, or array of files
 * @returns {Array.<string>} The list of files in the directory
 */

function readdirSyncExclude(path, exclude) {
  if (typeof exclude === 'string') exclude = [ exclude ];
  return difference(readdirSync(path), exclude);
}

const nullifier = {
  open: '',
  close: ''
};

/**
 * @description
 * Explicitly disables the action of chalk, preventing color from being added
 * @returns {boolean} Returns false if chalk was already disabled
 */

function neutralizeColor() {
  if (!neutralizeColor.cache) {
    neutralizeColor.cache = clone(chalk.styles);
    Object.keys(chalk.styles).forEach(function(v) {
      Object.assign(chalk.styles[v], nullifier);
    });
    return true;
  }
  return false;
}

/**
 * @description
 * Re-enable chalk if it was previously disabled
 * @returns {boolean} Returns false if chalk was already enabled
 */

function reenableColor() {
  if (!neutralizeColor.cache) return false;
  chalk.styles = neutralizeColor.cache;
  delete neutralizeColor.cache;
  return true;
}

const colorRe = /\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]/g;

/**
 * @description Removes color codes from string using regular expression
 * @param {string} str The string to remove color codes from
 * @returns {string} The string with color codes removed
 */

function stripColor(str) {
  return str.replace(colorRe, '');
}

/**
 * @description
 * Splices out an event handler from an EventEmitter instance that does not implement EventEmitter#off
 * @returns {void}
 */

function spliceHandler(evt, fn) {
  if (Array.isArray(this._events[evt])) {
    let idx = this._events[evt].indexOf(fn);
    if (!~idx) return;
    this._events[evt].splice(idx, 1);
    return;
  }
  if (typeof this._events[evt] === 'function') delete this._events[evt];
}

/** @description Extracts the name from the filepath, without extension
 * @param {string} path
 * @returns {string} The name
 * @example
 * filepathName('/etc/ssl/openssl.cnf')
 * // 'openssl'
 */

function filepathName(path) {
  return parse(path).name;
}

/** @description Extracts the base from the filepath, without directory
 * @param {string} path
 * @returns {string} The base
 * @example
 * filepathBase('/etc/ssl/openssl.cnf');
 * // 'openssl.cnf'
 */

function filepathBase(path) {
  return parse(path).base;
}

function endpoint(filename) {
  return join('/', filepathName(filename));
}


module.exports = {
  endpoint,
  filepathBase,
  filepathName,
  makeRouter,
  loadRoutesSync,
  loadChildrenSync,
  readdirSyncExclude,
  neutralizeColor,
  reenableColor,
  stripColor,
  spliceHandler
};
