"use strict";

import { Client } from 'irc';
import injectPrototype from '../lib/util/inject-prototype';
import { spliceHandler } from '../lib/util';
import { EventEmitter } from 'events';
import { bold, cyan, yellow } from 'chalk';
import { inspect } from 'util';
import { join, parse } from 'path';
import { lstatSync, readdirSync, existsSync } from 'fs';
import { mapSeries } from 'async';
import { forOwn } from 'lodash';
import log from '../lib/log';

const { isArray } = Array;
const { create, keys } = Object;

const { on: eveOn, emit: eveEmit } = EventEmitter.prototype;

function IRCClient(config = {}) {
  if (!(this instanceof IRCClient)) return new IRCClient(config);
  EventEmitter.call(this);
  this.config = config;
  if (!config.server) config.server = 'irc.freenode.net';
  if (!config.nick) config.nick = process.title;
  if (config.channels) {
    if (!isArray(config.channels)) config.channels = [config.channels];
  } else config.channels = ['#wonderbreadenterprises'];
  log.eventLnDecor('init', `${bold(yellow(`{${process.title}}`))} ${cyan('deploying...')}`)
  if (config.debug) {
    let emit = Client.prototype.emit;
    Client.prototype.emit = function () {
      log.debugLnDecor(inspect(Array.from(arguments), { depth: null, colors: true }));
      emit.apply(this, arguments);
    };
  }
  this._client = new Client(config.server || 'irc.freenode.net', config.nick || process.title, {
    channels: config.channels || ['#wonderbreadenterprises']
  });
  let self = this;
  let connected;
  this.on('connect', onConnect);
  this.bindToLog();
  this.plugins = create(null);
  this.on('join', onJoin);
  function onConnect() {
     self.off('connect', onConnect);
     connected = true;
     log.eventLnDecor('connect', 'connected to server');
  }
  function onJoin() {
     self.off('join', onJoin);
     log.eventLnDecor('join', 'joined channel');
     self.send('[[ wonderbot initializing ]]');
     self.loadPlugins();
     self.loadCommands();
  }
}

injectPrototype(IRCClient.prototype, EventEmitter.prototype);

const pluginHelp = () => {
  return this.error('usage: plugin [load|unload|list]');
};

const IRCClientFunctions = {
  log(msg) {
    this.send(msg);
    log.eventLnDecor('chat', msg);
  },
  error(msg) {
    this.send('error: ');
    log.errorLn(msg);
  },
  on() {
    eveOn.apply(this._client, arguments);
  },
  off() {
    spliceHandler.apply(this._client, arguments);
  },
  serverOn() {
    eveOn.apply(this, arguments);
  },
  loadPlugins() {
    let pluginNames;
    var thisServer = this, self = this;
    if ((pluginNames = keys(this.plugins)).length) {
      this.log('~~ reloading plugins');
      mapSeries(pluginNames, (v, next) => {
        try {
          this.plugins[v].unmount((err) => {
              if (!err) delete require.cache[require.resolve(join(__dirname, '..', 'plugins', v))];
              return next(err);
          });
          if (!this.plugins[v].unmount.length) next();
        } catch (e) {
          delete require.cache[require.resolve(join(__dirname, '..', 'plugins', v))];
          e.plugin = v;
          next(e);
        }
      }, (err) => {
        if (err) {
          handleUnloadError.call(this, err, err.plugin);
        } else requireAndMountAll(self, handleMount);
      });
    } else requireAndMountAll(self, handleMount);
    function handleMount(err) {
      if (err) {
        thisServer.log(`~~! plugin mount error in '${err.plugin || 'unknown'}'`);
        thisServer.log(err.message);
        log.errorLn(err.stack);
      } else thisServer.log('~~ plugins loaded');
    }
  },
  loadCommands() {
    this.addChatListener((from, message) => {
      let split = message.split(/\s+/);
      if (split[0] === 'plugin') {
        if (split.length === 1)
          pluginHelp.call(this);
        else if (split[1] === 'list') this.logPluginList();
        else if (split[1] === 'load') {
          if (split.length === 2) this.loadPlugins();
          else split.slice(2).forEach((v) => {
            this.loadPlugin(v);
          });
        } else if (split[1] === 'unload') {
          if (split.length === 2) forOwn(this.plugins, (value, key) => {
            this.unloadPlugin(key);
          });
          else split.slice(2).forEach((v) => {
            this.unloadPlugin(v);
          });
        } else pluginHelp.call(this);
      } else if (split[0] === 'unload') {
        if (split.length > 1) {
          split.slice(1).forEach((v) => {
            this.unloadPlugin(v);
          });
        }
      }
    });
  },
  unloadPlugin(plugin) {
    if (this.plugins[plugin]) {
      this.log(`~~ unmounting '${plugin}'`);
      try {
        this.plugins[plugin].unmount((err) => {
          handleUnloadError.call(this, err, plugin);
        });
        if (!this.plugins[plugin].unmount.length) {
          handleUnloadError.call(this, null, plugin);
        }
      } catch (e) {
        handleUnloadError.call(this, e, plugin);
      }
    } else {
      this.log(`~~! no plugin '${plugin}' active`);
    }
  },
  loadPlugin(plugin) {
      let self = this;
      if (this.plugins[plugin]) {
        this.log(`~~ '${plugin}' already mounted`);
        try {
          this.plugins[plugin].unmount((err) => {
            handleUnloadError.call(this, err, plugin);
            if (!err) requireAndMount(this, plugin, handleMount);
          });
          if (!this.plugins[plugin].unmount.length) {
            handleUnloadError.call(this, null, plugin);
            requireAndMount(this, plugin, handleMount);
          }
        } catch (e) {
          handleUnloadError.call(this, e, plugin);
        }
      } else requireAndMount(this, plugin, handleMount);
      function handleMount(err) {
        if (err) {
          self.log(`~~! plugin mount error in '${err.plugin || 'unknown'}'`);
          self.log(err.message);
          log.errorLn(err.stack);
        } else self.log('~~ plugins loaded');
      }
  },
  send(name, msg) {
    if (!msg) {
      msg = name;
      this.config.channels.forEach((v) => {
        this._client.say(v, msg);
      });
    } else this._client.say(name, msg);
  },
  addChatListener(fn) {
    this.config.channels.forEach((v) => {
      this.on(`message${v}`, fn);
    });
  },
  removeChatListener(fn) {
    this.config.channels.forEach((v) => {
      this.off(`message${v}`, fn);
    });
  },
  bindToLog() {
    this.on('error', (err) => {
      log.errorLn(err.message);
      log.debugLnDecor(err.stack);
    });
  },
  logPluginList() {
    this.log(`~~ available plugins`);
    readdirSync(join(__dirname, '..', 'plugins')).forEach((value) => {
      let parts = parse(value);
      if (parts.ext === '.js' || lstatSync(join(__dirname, '..', 'plugins', value)).isDirectory()) this.log(` * ${parts.name} ${this.plugins[parts.name] && '(active)' || ''}`);
    });
  }
};
function handleUnloadError(err, plugin) {
        if (err) {
          this.log(`~~! ${plugin} unmount error: ${err.message}`);
          this.log(` ~! WARNING: forcing plugin shutdown`);
          log.processError(err);
        }
        delete require.cache[require.resolve(join(__dirname, '..', 'plugins', plugin))];
}

function handleErr(err, plugin) {
    if (err) {
      err.plugin = plugin;
      this.log(`~~! error loading '${err.plugin}': ${err.message}`);
      log.processError(err);
    }
}

function requireAndMountAll(server, cb) {
  let pluginDir;
  let plugins = readdirSync((pluginDir = join(__dirname, '..', 'plugins')))
    .filter((v) => {
      return parse(v).ext === '.js' || lstatSync(join(pluginDir, v)).isDirectory();
    }).map((v) => {
      return parse(v).name;
    });
  mapSeries(plugins, (v, next) => {
    server.plugins[v] = require(join(__dirname, '..', 'plugins', v))(server);
    try { 
      server.plugins[v].mount((err) => {
        if (!err) server.log(` ** '${v}' loaded`);
        else {
          handleErr.call(server, err, v);
        }
        next();
      });
      if (!server.plugins[v].mount.length) {
        server.log(` ** '${v}' loaded`);
        next();
      }
    } catch (e) {
      handleErr.call(server, e, v);
      next();
    }
  }, (err) => {
    cb(err);
  });
}

function requireAndMount(server, plugin, cb) {
  if (!existsSync(join(__dirname, '..', 'plugins', plugin + '.js')) && !existsSync(join(__dirname, '..', 'plugins', plugin))) {
    server.log(` ** plugin '${plugin}' not found`);
    server.logPluginList();
  } else {
    try {
      server.plugins[plugin] = require(join(__dirname, '..', 'plugins', plugin))(server);
      server.plugins[plugin].mount((err) => {
        if (!err) server.log(` ** '${plugin}' loaded`);
        if (err) err.plugin = plugin;
        cb(err);
      });
      if (!server.plugins[plugin].mount.length) {
        server.log(` ** '${plugin}' loaded`);
        cb();
      }
    } catch (e) {
        e.plugin = plugin;
        cb(e);
    }
  }
}


Object.assign(IRCClient.prototype, IRCClientFunctions);

module.exports = IRCClient;

