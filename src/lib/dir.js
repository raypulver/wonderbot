"use strict";

import { join } from 'path';
import { homedir } from 'os';
import { writeFileSync, readFileSync, existsSync } from 'fs';
import pathExists from 'path-exists';
import mkdirp from 'mkdirp';
import log from './log';

function getHomeDir() {
  return join(homedir(), `.${process.title}`);
}

function getConfigPath() {
  return join(getHomeDir(), 'config.json');
}

function getConfig() {
  return require(getConfigPath());
}

function maybeMakeHomeDir() {
  let home, cfg;
  try {
    if (!pathExists.sync((home = getHomeDir()))) {
      mkdirp.sync(home);
    }
    if (!existsSync((cfg = getConfigPath()))) {
      writeFileSync(cfg, readFileSync(join(__dirname, '..', '..', 'presets', 'config.json')));
    }
  } catch (e) {
    console.error(e.stack);
    process.exit(1);
  }
}

module.exports = {
  getConfig,
  maybeMakeHomeDir,
  getHomeDir
}
