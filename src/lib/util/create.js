/** @module dist/lib/util/create
 * @description Create an object with a given prototype
 */

"use strict";

import mapOwn from './map-own';

const { create, getOwnPropertyDescriptor } = Object;

module.exports = function (proto, obj) {
  return create(proto, mapOwn(obj, (v, key, obj) => {
    return getOwnPropertyDescriptor(obj, key);
  }));
};
