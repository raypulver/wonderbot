/** @module dist/lib/util/map-own
 * @description Map objects own properties to new object
 */

"use strict";

import { forOwn } from 'lodash';

/** @description Map object to a new object, similar to Array#map
 * @param {Object} object The object to map
 * @param {Function} cb The callback function
 * @returns {Object} New object
 */

const { create, getPrototypeOf } = Object;

function mapOwn(object, cb) {
  let retval = create(getPrototypeOf(object));
  forOwn(object, function (val, key) {
    retval[key] = cb(val, key, object);
  });
  return retval;
}

module.exports = mapOwn;
