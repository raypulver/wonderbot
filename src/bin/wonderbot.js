"use strict";

import { parse } from 'path';

process.title = parse(process.argv[1]).name;

import IRCClient from '../lib/client';
import Getopt from 'node-getopt';
import log from '../lib/log';
import { cyan, green, yellow, gray, bold } from 'chalk';
import { EOL } from 'os';
import { maybeMakeHomeDir, getConfig } from '../lib/dir';
import packageJson from '../../package';

const { exit } = process;

let getopt = new Getopt([
  ['p', 'port=PORT', 'http port'],
  ['d', 'debug', 'enable debugging output'],
  ['h', 'help', 'display this help'],
  ['v', 'version', 'display version number']
]);

getopt.setHelp(`${yellow(bold('Usage:'))} ${yellow(bold(process.title))} ${yellow(bold('[-dvh] [-p PORT]'))}${EOL}${yellow('Options are:')}${EOL}${gray(bold('[[OPTIONS]]'))}`);

let opts = getopt.parseSystem();

if (opts.options.help) {
  getopt.showHelp();
  exit(0);
}

if (opts.options.version) {
  log(packageJson.version);
  exit(0);
}

maybeMakeHomeDir();

let config = getConfig();

config.debug = opts.options.debug;
log.setDebugging(opts.options.debug);
if (opts.options.port) config.port = opts.options.port;

let irc = IRCClient(config);

irc.addChatListener((from, message) => {
  log.infoLn(`${from} => ${message}`);
});

