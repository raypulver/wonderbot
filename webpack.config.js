"use strict";

const path = require('path'),
  webpack = require('webpack'),
  assetPath = path.join(__dirname, 'assets'),
  publicPath = path.join(__dirname, 'public');

module.exports = {
  entry: path.join(assetPath, 'main.js'),
  output: {
    path: publicPath,
    filename: 'bundle.js',
    sourceMapFilename: 'bundle.js.map'
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      compress: { warnings: false },
      output: {
        comments: false
      },
      sourceMap: true
    })
  ],
  module: {
    loaders: [{
      test: /(?:\.js$)/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        presets: ['es2015']
      }
    }, {
      test: /\.css$/,
      loader: 'style!css'
    }, {
      test: /\.less$/,
      loader: 'style!css!less',
    }, {
      test: /\.gif/,
      loader: 'url?limit=10000&mimetype=image/gif'
    }, {
      test: /\.jpe?g/i,
      loader: 'url?limit=340000&mimetype=image/jpg'
    }, {
      test: /\.png/i,
      loader: 'url?limit=340000&mimetype=image/png'
    }, {
      test: /\.woff|woff2$/,
      loader: 'url?limit=10000&mimetype=application/font-woff'
    }, {
      test: /(?:\.(?:ttf|eot|svg))(\?.*$|$)/,
      loader: 'file'
    }]
  }
};
